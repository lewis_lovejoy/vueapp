# URL Shortening Full Stack

This app takes a URL and returns a shorthand version i.e. an 8 character (lowercase-alphanumeric) version of the URL.
The same URL will always give the same short version, and different urls will give different short versions.
(unless the database is reset)

Run with: 

`docker-compose up`

then point your browser at:

`http://localhost:8080`

to run the unit tests (`yarn install` in both directories first):

`yarn test:unit` - for client

`yarn test` - for API

### Consist of:
- a docker compose to run everything (with 2 dockerfiles and 3 instances)
- backend services in /api using nodejs+express (with a link to mongodb)
- SPA in /vueclient using vue

All using typescript

/api and /vueclient are independent and run separately (both in DEV mode).
This is to show capabilities - not to run/generate a production system.

### Notes:
Some rather long notes:
- This is not PRODUCTION grade code - it's DEV code to show ability (performance/tidying have not been done)
- I'm not building the VUE app and serving it through a server - I'm jst using dev mode
- It contains a LOT of 'boiler plate' that hasn't been cleaned/optimised etc. including:
    - docker stuff - just the basics - aimed at DEV not PRODUCTION
    - client - basic CLI created app - so logo etc. are all still there
    - api - simple node express app
    - node.js - current docker version - for no particular reason
- Lots of 'edge cases' are missing from tests and core code - this is a basic solution
- No clever CSS or Front-end (very basic css) - including: No SEO, No accessibility
- No performance tuning or tidying
- I haven't added any e2e test - only basic unit tests. Also, No TDD - just basic tests (after the code) - and Low (ish) code coverage - just the main 'functions'
- I've done everything as a single commit to the repo - I'd normally split this and commit each feature
- The API server will throw errors for bad stuff rather than return gracefully
- Using just RAW mongodb calls - could use mongoose or some other wrapper to verify schema etc. 
- I've left in console.logs and NOT used a logging framework
- No 'de-bounce' on the submit of new URLs on the form - or any 'wait' indication (so bad usability)
- CORS is enabled for everything so dev works - so a security hole
- I haven't put a 'window' on the results - so this will fail for a large database. Also relying
on the browser scroll - so bad usability
- simple algorithm for creating tiny url just using zero padded base36 (with a unique number from the DB) - it works,
but the url looks weird as it basically counts at the beginning - but gives the largest number of possible 
results

