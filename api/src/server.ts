import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import MongoClientDB, {Collection, MongoError, MongoClient} from 'mongodb';
import addUrl from './core';

const database = MongoClientDB.MongoClient;
const app = express();

const databaseURL = "mongodb://172.17.0.1:27017/";

app.set('port', process.env.PORT || 3000);
app.set('host', process.env.HOST || '0.0.0.0');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const corsOptions = {'origin': '*'}

database.connect(databaseURL, (err:MongoError, client: MongoClient) => {
    if (err) {
        return console.error(err);
    }
    console.log('Connected to Database');

    const db = client.db('example');
    const urlsCollection = db.collection('urls');
    const counterCollection = db.collection('counter');

    app.get('/', (req, res) => {
        res.send('Just a test the API server is up');
    });

    app.options('/url', cors(corsOptions));
    app.post('/url', cors(corsOptions), async (req, res) => {
        const { url } = req.body;
        if (url && typeof url === 'string') {
            const tinyUrl = await addUrl((<string> url).toLowerCase(), urlsCollection, counterCollection);
            res.send({tinyUrl});
            return;
        }
        res.sendStatus(400);
    });

    app.get('/url', cors(corsOptions), async (req, res) => {
        const urlCollectionCursor = urlsCollection.find();
        const allResults = [];
        while (await urlCollectionCursor.hasNext()) {
            const row = await urlCollectionCursor.next();
            allResults.push({ original:row.original, tinyUrl: row.tinyUrl});
        }
        res.send(allResults);
    });
})

app.listen(app.get('port'), app.get('host'), () => {
    console.log(`\nServer listening at http://${app.get('host')}:${app.get('port')}`);
});
