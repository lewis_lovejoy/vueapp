/**
 * @jest-environment mongodb
 */
import { MongoClient } from 'mongodb';

import addUrl, {getBase36Padded,getNextSequenceValue} from './core';

describe('CORE getBase36Padded api tests', () => {
    it('pads to 8 chars', () => {
        expect(getBase36Padded(0)).toBe('00000000');
    });
    it('returns base36 encoding', () => {
        expect(getBase36Padded(808334348993)).toBe('abcdefgh');
        expect(getBase36Padded(2097984592401)).toBe('qrstuvwx');
        expect(getBase36Padded(1453159470697)).toBe('ijklmnop');
        expect(getBase36Padded(2740632931770)).toBe('yz123456');
        expect(getBase36Padded(566507652304)).toBe('78901234');
    });
});

describe('CORE getNextSequenceValue api tests', () => {
    let client : any;
    let db : any;

    beforeAll(async () => {
        client = await MongoClient.connect((<any>global).MONGO_URI);
        db = await client.db((<any>global).MONGO_DB_NAME);
    });

    afterAll(async () => {
        await client.close();
    });

    beforeEach(async () => {
        // Reset the database before each test
        await db.dropDatabase();
    });

    it('counter to return sequence of digits', async () => {
        const collection = db.collection('counter');
        expect(await getNextSequenceValue(collection)).toBe(0);
        expect(await getNextSequenceValue(collection)).toBe(1);
        expect(await getNextSequenceValue(collection)).toBe(2);
    });
});

describe('CORE addUrl api tests', () => {
    let client : any;
    let db : any;

    beforeAll(async () => {
        client = await MongoClient.connect((<any>global).MONGO_URI);
        db = await client.db((<any>global).MONGO_DB_NAME);
    });

    afterAll(async () => {
        await client.close();
    });

    beforeEach(async () => {
        // Reset the database before each test
        await db.dropDatabase();
    });

    it('addURL to return tinyurl without duplicates', async () => {
        const counter = db.collection('counter');
        const url = db.collection('url');
        expect(await addUrl('test', url, counter)).toBe('http://pbid.io/00000000');
        expect(await addUrl('test', url, counter)).toBe('http://pbid.io/00000000');
        expect(await addUrl('test2', url, counter)).toBe('http://pbid.io/00000001');
        expect(await addUrl('test3', url, counter)).toBe('http://pbid.io/00000002');
        expect(await addUrl('test', url, counter)).toBe('http://pbid.io/00000000');
    });
});
