import {Collection} from "mongodb";

export const getNextSequenceValue = async (counterCollection : Collection) : Promise<number> => {
    const sequenceDocument = await counterCollection.findOneAndUpdate(
        { _id: 'counter' },
        { $inc:{ sequence_value:1 }}
    );
    if (!sequenceDocument.value) {
        await counterCollection.insertOne({ _id: 'counter', sequence_value: 1 });
        return 0;
    }
    return sequenceDocument.value.sequence_value;
}

export const getBase36Padded = (counter : number) : string => {
    const newString = `00000000${counter.toString(36)}`;
    return newString.substr(newString.length-8);
}

const addUrl = async (urlToConvert : string, urlsCollection : Collection, counterCollection : Collection) : Promise<string> => {
    const newUrlObject = await urlsCollection.findOne({ original: urlToConvert });
    if (newUrlObject) {
        return newUrlObject.tinyUrl;
    }
    const newUniqueCount = await getNextSequenceValue(counterCollection);
    const tinyUrl = `http://pbid.io/${getBase36Padded(newUniqueCount)}`;
    await urlsCollection.insertOne({
        original: urlToConvert,
        tinyUrl
    });
    return tinyUrl;
}

export default addUrl;
