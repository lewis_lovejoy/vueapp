import { shallowMount, mount } from '@vue/test-utils';
import AddTinyUrl from '@/components/AddTinyUrl.vue';
import { Vue } from 'vue-property-decorator';

jest.mock('@/services/DataService');

describe('AddTinyURL.vue', () => {
  let dataService: any;
  beforeEach(() => {
    dataService = require('@/services/DataService');
  });

  it('renders top level', () => {
    const msg = 'Add New URL URL to encode  Submit';
    const wrapper = shallowMount(AddTinyUrl);
    expect(wrapper.text()).toMatch(msg);
  });

  it('renders matches snapshot', () => {
    const wrapper = mount(AddTinyUrl);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders matches snapshot after successful submit', async () => {
    dataService.setCreateResponse({ tinyUrl: 'tinyTestUrl' });

    const wrapper = mount(AddTinyUrl);
    const button = wrapper.find('button');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper).toMatchSnapshot();
  });

  it('renders matches snapshot after failure submit', async () => {
    dataService.setCreateResponse(null);

    const wrapper = mount(AddTinyUrl);
    const button = wrapper.find('button');
    await button.trigger('click');
    await Vue.nextTick();
    expect(wrapper).toMatchSnapshot();
  });
});
