import { shallowMount, mount } from '@vue/test-utils';
import ListTinyUrl from '@/components/ListTinyUrl.vue';
import {Vue} from "vue-property-decorator";

jest.mock('@/services/DataService');

describe('ListTinyURL.vue', () => {
  let dataService: any;
  beforeEach(() => {
    dataService = require('@/services/DataService');
  });

  it('renders top level when blank', () => {
    const msg = 'TinyURL List Original URLTiny URLLink';
    const wrapper = shallowMount(ListTinyUrl);
    expect(wrapper.text()).toMatch(msg);
  });

  it('render matches snapshot when blank', () => {
    const wrapper = mount(ListTinyUrl);
    expect(wrapper).toMatchSnapshot();
  });

  it('render matches snapshot when has data', async () => {
    dataService.setData([
      { original:'a', tinyUrl: 'a'},
      { original:'b', tinyUrl: 'b'}
    ]);
    const wrapper = mount(ListTinyUrl);
    await Vue.nextTick();
    //await wrapper.setData({});
    expect(wrapper).toMatchSnapshot();
  });
});
