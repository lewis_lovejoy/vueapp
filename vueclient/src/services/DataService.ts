import http from '../http-common';

class DataService {
  static getAll() {
    return http.get('/url');
  }

  static create(data: any) {
    return http.post('/url', data);
  }
}

export default DataService;
