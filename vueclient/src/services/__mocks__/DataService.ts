//const DataService = jest.createMockFromModule('./DataService');

class DataService {
    static fakeData = [];
    static createResponse = null;

    static setData(newData: []) {
        DataService.fakeData = newData;
    }

    static setCreateResponse(cr: any) {
        DataService.createResponse = cr;
    }

    static getAll() {
        return new Promise((resolve, reject) => { resolve({data: DataService.fakeData}) });
    }

    static create(data: any) {
        return new Promise((resolve, reject) => { resolve({data: DataService.createResponse }) });
    }
}

module.exports = DataService;
