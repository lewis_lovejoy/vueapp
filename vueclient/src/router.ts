import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    alias: '/listurls',
    name: 'listurls',
    component: () => import('./components/ListTinyUrl.vue'),
  },
  {
    path: '/addurl',
    name: 'addurl',
    component: () => import('./components/AddTinyUrl.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
